package org.example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Library bookLibrary = new Library();

        Book book1 = new Book("Crime And Punishment", "Fyodor Dostoevsky",
                "psychological literary fiction", 1866);
        Book book2 = new Book("The Girl on the Train", "Paula Hawkins",
                "thriller", 2015);
        Book book3 = new Book("Educated", "Tara Westover",
                "memoir", 2018);
        Book book4 = new Book("The Catcher in the Rye", "J.D. Salinger",
                "coming-of-age fiction", 1951);
        Book book5 = new Book("One Hundred Years of Solitude", "Gabriel García Márquez",
                "magical realism", 1967);
        Book book6 = new Book("The Night Circus", "Erin Morgenstern",
                "magical realism", 2011);

        System.out.print("Added: -> " + book1);
        bookLibrary.addBookToLibrary(book1);
        System.out.println();
        System.out.print("Added: -> " + book2);
        bookLibrary.addBookToLibrary(book2);
        System.out.println();
        System.out.print("Added: -> " + book3);
        bookLibrary.addBookToLibrary(book3);
        System.out.println();
        System.out.print("Added: -> " + book4);
        bookLibrary.addBookToLibrary(book4);
        System.out.println();
        System.out.print("Added: -> " + book5);
        bookLibrary.addBookToLibrary(book5);
        System.out.println();
        System.out.print("Added: -> " + book6);
        bookLibrary.addBookToLibrary(book6);
        System.out.println();
        System.out.println();
        System.out.println("The number of books in the library: " + bookLibrary.getTheNumberOfBooks());

        System.out.println();
        Book bookToRemove = new Book("Crime And Punishment", "Fyodor Dostoevsky",
                "psychological literary fiction", 1866);
        System.out.println("Removed: -> " + bookToRemove);
        bookLibrary.removeBookFromLibrary(bookToRemove);

        Book bookToRemove2 = new Book("The Girl on the Train", "Paula Hawkins",
                "thriller", 2015);
        System.out.println("Removed: -> " + bookToRemove2);
        bookLibrary.removeBookFromLibrary(bookToRemove2);
        System.out.println();

        System.out.println("The number of books in the library: " + bookLibrary.getTheNumberOfBooks());
        System.out.println();
        System.out.println("All books in the library sorted based on title");
        for(Book b : bookLibrary.getAllBooksSortedByTitle()){
            System.out.println("- " + b);
        }
        System.out.println();
        System.out.println("All books in the library sorted based on author");
        for(Book b : bookLibrary.getAllBooksSortedByAuthor()){
            System.out.println("- " + b);
        }
        System.out.println();
        System.out.println("All books in the library sorted based on publication year");
        for(Book b : bookLibrary.getAllBooksSortedByPublicationYear()){
            System.out.println("- " + b);
        }
        System.out.println();
        System.out.println("Search Book with the title of \"The Catcher in the Rye\"");
        for(Book b : bookLibrary.findByTitle("The Catcher in the Rye")){
            System.out.println("- " + b);
        }

        System.out.println();
        System.out.println("Search Book with the genre of \"magical realism\"");
        for(Book b : bookLibrary.findByGenre("magical realism")){
            System.out.println("- " + b);
        }

        System.out.println();
        System.out.println("Search Book with the author of \"Tara Westover\"");
        for(Book b : bookLibrary.findByAuthor("Tara Westover")){
            System.out.println("- " + b);
        }

        System.out.println();
        System.out.println("Average publication year of all books: " + bookLibrary.getAverageOfPublicationYears());
    }
}