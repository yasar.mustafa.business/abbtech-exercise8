package org.example;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Library {

    private ArrayList<Book> bookLibrary = new ArrayList<>();

    public void addBookToLibrary(Book book){
        bookLibrary.add(book);
    }

    public void removeBookFromLibrary(Book book){
        bookLibrary.remove(book);
    }

    public List<Book> findByTitle(String title){
        return bookLibrary.stream().filter((t) -> t.getTitle().equals(title))
                .collect(Collectors.toList());
    }

    public List<Book> findByAuthor(String author){
        return bookLibrary.stream().filter((t) -> t.getAuthor().equals(author))
                .collect(Collectors.toList());
    }

    public List<Book> findByGenre(String genre){
        return bookLibrary.stream().filter((t) -> t.getGenre().equals(genre))
                .collect(Collectors.toList());
    }

    public List<Book> getAllBooksSortedByTitle(){
        return bookLibrary.stream().sorted(Comparator.comparing(Book::getTitle))
                .collect(Collectors.toList());
    }

    public List<Book> getAllBooksSortedByAuthor(){
        return bookLibrary.stream().sorted(Comparator.comparing(Book::getAuthor))
                .collect(Collectors.toList());
    }

    public List<Book> getAllBooksSortedByPublicationYear(){
        return bookLibrary.stream().sorted(Comparator.comparing(Book::getPublicationYear))
                .collect(Collectors.toList());
    }

    public double getAverageOfPublicationYears(){
        return bookLibrary.stream().mapToInt(Book::getPublicationYear)
                .average().orElse(0);
    }

    public int getTheNumberOfBooks(){
        return bookLibrary.size();
    }
}
